using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Ant : MonoBehaviour
{
    [SerializeField]
    private GameObject pheromoneGO;

    [SerializeField] //��� �������
    private bool isBrownianMotionNedeed = false;
    private bool isTimerDone = true;
    [SerializeField] //��� �������
    private bool holdingFood = false;
    [SerializeField] //��� �������
    private bool activeFood = false;
    private float speed;
    private int lifetime;

    private Vector3 antHillPose;
    [SerializeField] //��� �������
    private Vector3 moveDirection;
    [SerializeField] //��� �������
    private Vector3 activeFoodPose;

    private Transform tr;

    private Anthill anthill;

    private MapController mapController;

    private NavMeshAgent agent;

    private TrailRenderer trail;

    public MapController MapController
    {
        set
        {
            mapController = value;
        }
    }

    public int Lifetime
    {
        get => lifetime;
        set
        {
            lifetime = value;
            StartCoroutine(LifeTimer(lifetime));
        }
    }
    public Anthill AntHill
    {
        get => anthill;
        set
        {
            anthill = value;
            antHillPose = anthill.GetComponent<Transform>().position;
        }
    }

    public float Speed
    {
        get => speed;
        set
        {
            speed = value;
        }
    }

    public bool HoldingFood
    {
        get => holdingFood;
        set
        {
            holdingFood = value;
            gameObject.GetComponentsInChildren<MeshRenderer>()[2].enabled = holdingFood;
        }
    }

    private void Start()
    {
        SetAgentPropertys();
        tr = transform;
        trail = gameObject.GetComponentInChildren<TrailRenderer>();
        moveDirection = Vector3.zero;
        StartCoroutine(InfiniteMotion());
        StartCoroutine(InitialMotion());
        StartCoroutine(BrownianMotion());
        mapController.AntCount++;
    }

    private void SetAgentPropertys()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.speed = speed;
    }

    private void Update()
    {
        //   Debug.Log("DeltaPos: " + agent.remainingDistance + "; Speed: " + agent.velocity); //velocity - Vector3
        if (holdingFood)
        {
            if (moveDirection != antHillPose)
            {
                moveDirection = antHillPose;
            } 
        }
        else
        {
            if (activeFood)
            {
                if (moveDirection != activeFoodPose)
                {
                    moveDirection = activeFoodPose;
                }
            }
            //new Vector3(activeFoodPose.x, tr.position.y, activeFoodPose.z)
        }
    }

    public void FoodFound(Food food)
    {
        activeFood = true;
        food.Size -= 1;
        activeFoodPose = food.Pose;
        isBrownianMotionNedeed = false;
    }

    public void FoodInPileIsOver()
    {
        isBrownianMotionNedeed = true;
        activeFood = false;
    }

    private IEnumerator InfiniteMotion()
    {
        agent.SetDestination(moveDirection);
        Vector3 currentDest = moveDirection;
        yield return new WaitUntil(() => agent.remainingDistance == 0 || currentDest != moveDirection);
        StartCoroutine(InfiniteMotion());
    }

    private IEnumerator InitialMotion()
    {
        moveDirection = new Vector3(Random.Range(-49, 50), tr.position.y, Random.Range(-49, 50));
        yield return new WaitUntil(() => agent.remainingDistance == 0 || holdingFood);
        if (!holdingFood)
        {
            isBrownianMotionNedeed = true;          
        }
    }

    private IEnumerator BrownianMotion()
    {
        if (isBrownianMotionNedeed)
        {
            float x = tr.position.x + Random.Range(-18, 18);
            float z = tr.position.z + Random.Range(-18, 18);
            if (x > 49 || x < -49 || z > 49 || z < -49)
            {
                x = Random.Range(-40, 40);
                z = Random.Range(-40, 40);
            }
            moveDirection = new Vector3(x, tr.position.y, z);
            yield return new WaitUntil(() => agent.remainingDistance == 0 || !isBrownianMotionNedeed);
        }
        else
        {
            yield return new WaitUntil(() => isBrownianMotionNedeed);
        }
        StartCoroutine(BrownianMotion());
    }

    private IEnumerator MakePheromoneTrail()
    {
        yield return new WaitUntil(() => activeFood);
        
    }

    private IEnumerator LifeTimer(int timeToDie)
    {
        if (timeToDie != 0)
        {
            yield return new WaitForSecondsRealtime(timeToDie + Random.Range(-15, 16));
            mapController.AntCount--;
            Destroy(gameObject);
        }
    }
    /*/
    private void Update()
    {
        if (isBrownianMotionNedeed)
        {
            tr.Rotate(rotateDirection);
            if (isTimerDone)
            {
                StartCoroutine(SetRandomRotate());
            }
            isTimerDone = false;
        }
        else if (holdingFood)
        {
            tr.LookAt(new Vector3(antHillPose.x, tr.position.y, antHillPose.z));
        }
        else
        {
            tr.LookAt(new Vector3(activeFoodPose.x, tr.position.y, activeFoodPose.z));
        }
        CheckForReverseRotation();
    }

    private void FixedUpdate()
    {
        Move();
    }

    private void Move()
    {
        tr.Translate(moveDirection * Time.deltaTime * speed);
    } 

    private void CheckForReverseRotation()
    {
        if (tr.position.x > 49 || tr.position.x < -49 || tr.position.z > 49 || tr.position.z < -49)
        {
            tr.LookAt(new Vector3(0, tr.position.y, 0));
        }
    }

    public void FoodFound(Food food)
    {
        food.Size -= 1;
        activeFoodPose = food.Pose;
        isBrownianMotionNedeed = false;
    }

    public void FoodInPileIsOver()
    {
        isBrownianMotionNedeed = true;
    }

    IEnumerator SetRandomRotate()
    {
        int random = Random.Range(1, 4);
        rotateDirection = random == 1 ? Vector3.zero : random == 2 ? Vector3.up : Vector3.down;
        yield return new WaitForSecondsRealtime(Random.Range(0.5f, 1.2f));
        rotateDirection = Vector3.zero;
        yield return new WaitForSecondsRealtime(Random.Range(5, 12));
        isTimerDone = true;
    }

    IEnumerator InitialForwardMovement()
    {
        moveDirection = Vector3.forward;
        yield return new WaitForSecondsRealtime(1f);
        isBrownianMotionNedeed = true;
    }

    IEnumerator LifeTimer(int timeToDie)
    {
        yield return new WaitForSecondsRealtime(timeToDie + Random.Range(-15, 16));
        mapController.AntCount--;
        Destroy(gameObject);
    }
    /*/
}
